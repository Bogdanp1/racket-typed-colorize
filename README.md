# Typed-Colorize

Typed Racket interface to the colorize package.


## About

With this package You can easily use the
[colorize](https://github.com/yanyingwang/colorize)
package functions from inside Typed Racket code.


## Installation

### Raco

Use raco to install Typed-Colorize from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user typed-colorize
```

### Req

Use Req to install Typed-Colorize from its project directory.

``` sh
raco req --everything --verbose
```


## License

The same as the `colorize` package, so `Apache-2.0`.

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the Apache-2.0 License

SPDX-License-Identifier: Apache-2.0
